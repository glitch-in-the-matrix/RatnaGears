import Vue from 'vue'
import App from './App.vue'
// import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import firebase from 'firebase'
import AlertCmp from './activities/alert.vue'
// import './plugins/vuetify' // library for using Vuetify
// import vuetify from 'vuetify'
import { router } from './router/' // import router object
import { store } from './rgrsp'
import vuetify from './plugins/vuetify'
import Vuetify from 'vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
require('firebase/auth')
// Import the styles directly. (Or you could add them via script tags.)
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(Vuetify)
// Vue.use(BootstrapVue)
Vue.component('app-alert', AlertCmp)

var config = {
  apiKey: 'AIzaSyDDVv0jVdK2xnowk7aZAXRq5Zta2NvPhyA',
  authDomain: 'ratna-gears.firebaseapp.com',
  databaseURL: 'https://ratna-gears.firebaseio.com',
  projectId: 'ratna-gears',
  storageBucket: 'ratna-gears.appspot.com',
  messagingSenderId: '611917228871',
  appId: '1:611917228871:web:50b403924cc239ad947a62',
  measurementId: 'G-1XQFCXZ41G'
}

new Vue({
  // use router object
  router,
  store,
  vuetify,
  created () {
    firebase.initializeApp(config)
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
  },
  render: h => h(App)
}).$mount('#app')
