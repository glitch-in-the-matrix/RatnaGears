// this is the main class of the router which will contain all the routes

// Import statements to get all required libraries
import Vue from 'vue'
import Router from 'vue-router' // library for creating Router object
// Import statements to get all required components for routing
import home from '../activities/homepage/homepage.vue'
import quotation from '../activities/quotation/quotation.vue'
import searchPage from '../activities/searchPage/searchPage.vue'
import quotationPreview from '../activities/quotationPreview/quotationPreview.vue'
import vendorPO from '../activities/vendor_PO/vendor_PO.vue'
import loginpage from '../activities/loginpage/loginpage.vue'
// eslint-disable-next-line camelcase
import preview_PO from '../activities/preview_PO/preview_PO.vue'
// eslint-disable-next-line camelcase
import customer_PO from '../activities/customer_PO/customer_PO.vue'
import AuthGuard from './auth-guard'
import addProcesses from '../activities/addProcesses/addProcesses.vue'
import adminPanel from '../activities/adminPanel/adminPanel.vue'
// eslint-disable-next-line camelcase
import vendor_PO_searchPage from '../activities/vendor_PO_searchPage/vendor_PO_searchPage.vue'
import landingPage from '../activities/landingPage/landingPage.vue'
import userManagement from '../activities/userManagement/userManagement.vue'
import bankDetailsManagement from '../activities/bankDetailsManagement/bankDetailsManagement.vue'
import clientDetailsManagement from '../activities/clientDetailsManagement/clientDetailsManagement.vue'

Vue.use(Router) // Using Router object in Vue

export const router = new Router({ // Exporting a constant object 'router' which is of Router type
  mode: 'history', // using history mode to maintain history stack of routes
  base: process.env.BASE_URL, // using 'base' variable as a baseURL
  routes: [ // this is the array which contains all the paths/routes
    { // this is the default route which redirects to route of 'redirect' property
      path: '/',
      redirect: '/loginpage'
    },
    { // this is the object which defines route and name for component 'activityWelcome'
      path: '/loginpage', // route of component
      name: 'loginpage', // name of component
      component: loginpage // object of component
    },
    { // this is the object which defines route and name for component 'activityWelcome'
      path: '/adminPanel', // route of component
      name: 'adminPanel', // name of component
      component: adminPanel, // object of component
      props: true,
      params: true,
      children: [
        {
          path: '/',
          redirect: '/userManagement'
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/userManagement', // route of component
          name: 'userManagement', // name of component
          component: userManagement, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/bankDetailsManagement', // route of component
          name: 'bankDetailsManagement', // name of component
          component: bankDetailsManagement, // object of component
          props: true,
          params: true
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/clientDetailsManagement', // route of component
          name: 'clientDetailsManagement', // name of component
          component: clientDetailsManagement, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        }
      ]
    },
    { // this is the object which defines route and name for component 'activityWelcome'
      path: '/landingPage', // route of component
      name: 'landingPage', // name of component
      component: landingPage, // object of component
      children: [
        {
          path: '/',
          redirect: '/home'
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/home', // route of component
          name: 'homePage', // name of component
          component: home, // object of component
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/quotation', // route of component
          name: 'quotation', // name of component
          component: quotation, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/search', // route of component
          name: 'searchPage', // name of component
          component: searchPage, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/quotationPreview', // route of component
          name: 'quotationPreview', // name of component
          component: quotationPreview, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/vendor_PO', // route of component
          name: 'vendor_PO', // name of component
          component: vendorPO, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/preview_PO', // route of component
          name: 'preview_PO', // name of component
          component: preview_PO, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/customer_PO', // route of component
          name: 'customer_PO', // name of component
          component: customer_PO, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        },
        { // this is the object which defines route and name for component 'activityWelcome'
          path: '/vendor_PO_searchPage', // route of component
          name: 'vendor_PO_searchPage', // name of component
          component: vendor_PO_searchPage, // object of component
          props: true,
          params: true,
          beforeEnter: AuthGuard
        }
      ]
    },
    { // this is the object which defines route and name for component 'activityWelcome'
      path: '/addProcesses', // route of component
      name: 'addProcesses', // name of component
      component: addProcesses, // object of component
      props: true,
      params: true
    }
  ]
})
