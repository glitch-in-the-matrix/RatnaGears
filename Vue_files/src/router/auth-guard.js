import { store } from '../rgrsp'

export default (to, from, next) => {
  if (store.getters.user) {
    next()
  } else {
    next('/loginpage')
  }
}
