import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    loading: false,
    error: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    }
  },
  actions: {
    signUserIn ({ commit }, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(function () {
          return firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        })
        .catch(function (error) {
          // Handle Errors here.
          commit('setLoading', false)
          commit('setError', error)
          // console.log(error)
        })
      var dataToSearch = { email: payload.email }
      axios
        .get('http://127.0.0.1:8000/admin_panel/saveAndRetrieveUsers/', { params: dataToSearch })
        .then(function (res) {
          const newUser = JSON.parse(res.data)
          // console.log(typeof payload.user)
          // console.log(payload.userData[0])
          // console.log(newUser[0])
          commit('setUser', newUser[0])
        })
        .catch(function (error) {
          commit('setLoading', false)
          commit('setError', error)
          // console.log(error)
        })
      // firebase.default.auth().signInWithEmailAndPassword(payload.email, payload.password)
      //   .then(
      //     user => {
      //       const newUser = {
      //         id: user.uid
      //       }
      //       commit('setUser', newUser)
      //     }
      //   )
      //   .catch(
      //     error => {
      //       commit('setLoading', false)
      //       commit('setError', error)
      //       console.log(error)
      //     }
      //   )
    },
    autoSignIn ({ commit }, payload) {
      // const signedUser = payload
      // commit('setUser', signedUser)
      var dataToSearch = { email: payload.email }
      axios
        .get('http://127.0.0.1:8000/admin_panel/saveAndRetrieveUsers/', { params: dataToSearch })
        .then(function (res) {
          const newUser = JSON.parse(res.data)
          // console.log(typeof payload.user)
          // console.log(payload.userData[0])
          // console.log(newUser[0])
          commit('setUser', newUser[0])
        })
        .catch(function (error) {
          commit('setLoading', false)
          commit('setError', error)
          // console.log(error)
        })
    },
    logout ({ commit }) {
      firebase.auth().signOut()
      commit('setUser', null)
      commit('setLoading', false)
    },
    resetPassword ({ commit }, payload) {
      firebase.auth().sendPasswordResetEmail(payload.email).then(function () {
        // Email sent.
      }).catch(function (error) {
        // An error happened.
        commit('setLoading', false)
        commit('setError', error)
        // console.log(error)
      })
    },
    clearError ({ commit }) {
      commit('clearError')
    },
    errorHandler ({ commit }, error) {
      commit('setError', error)
      // console.log(error)
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    }
  }
})
