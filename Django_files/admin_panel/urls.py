from django.urls import path
from .views import saveAndRetrieveUsers, deleteUsers, changeUserDetails, saveAndRetrieveBankDetails, deleteBankDetails,saveAndRetrieveCompanyDetails, deleteCompany, changeCompanyDetails

urlpatterns = [
    path('saveAndRetrieveUsers/', saveAndRetrieveUsers,name='saveAndRetrieveUsers'),
    path('deleteUsers/', deleteUsers,name='deleteUsers'),
    path('changeUserDetails/', changeUserDetails,name='changeUserDetails'),
    path('saveAndRetrieveBankDetails/', saveAndRetrieveBankDetails,name='saveAndRetrieveBankDetails'),
    path('deleteBankDetails/', deleteBankDetails,name='deleteBankDetails'),
    path('saveAndRetrieveCompanyDetails/', saveAndRetrieveCompanyDetails,name='saveAndRetrieveCompanyDetails'),
    path('deleteCompany/', deleteCompany, name='deleteCompany'),
    path('changeCompanyDetails/', changeCompanyDetails, name='changeCompanyDetails'),
]