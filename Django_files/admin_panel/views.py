from django.shortcuts import render
from mongoConnector import Connector as ct
from django.shortcuts import render,HttpResponse
from django.http import JsonResponse
from bson.json_util import dumps
import json
from django.views.decorators.csrf import csrf_exempt
import firebase_admin
from firebase_admin import credentials,auth

cred = credentials.Certificate("admin_panel/ratna-gears-firebase-adminsdk-81m3o-3064fe6fb8.json")
firebase_admin.initialize_app(cred)

# Create your views here.
@csrf_exempt
def saveAndRetrieveUsers(request):
    
    #for searching in db
    if request.method == 'GET':

        try:
            field = list(request.GET.dict().keys())[0]
            inputData = request.GET[field]
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_Users')
        if field == 'email':
            data = ct.findByEmail(inputData)
        else:
            # send error message if search parameter extraction failed
            e = 'Invalid data in request, failed to parse.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        data = dumps(list(data))
        #print (data)
        return JsonResponse(data,safe=False)

    # for saving in db
    if request.method == 'POST':

        # set database and collection
        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_Users')

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))


        if isPermited=='true' or isPermited==True:
            try:
                user = auth.create_user(email=finalData['email'], password=finalData['password'])
            except Exception as e:
                print(e)
                return JsonResponse({'status': 'false', 'message': str(e), 'code': '400'}, status=200, reason=str(e))
            finalData['_id'] = user.uid
            del finalData['password']
            del finalData['permission']
            try:
                ct.saveData(finalData)
            except:
                e = 'Database write User operation failed.'
                return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
            # return HttpResponse("Status code: 200 User Created Successfully.")
            return JsonResponse({'status':'false','message':'User Created Successfully', 'code': '200'}, status=200)
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def deleteUsers(request):
    #for deleting users
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_Users')
            auth.delete_user(finalData['_id'])
            ct.deleteById(finalData['_id'])
            return HttpResponse("Status code: 200 User Deleted Successfully.")
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def changeUserDetails(request):

    if request.method == 'POST':

        # set database and collection
        

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
            print(isPermited)
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            id = finalData['_id']
            del finalData['_id']
            del finalData['permission']
            try:
                ct.setDatabase('RG_Database')
                ct.setActiveCollection('RG_Users')
                ct.replaceUserDetailsById(id, finalData)
                return HttpResponse("Status code: 200 User Details Changed Successfully.")
            except:
                e = 'User details updation failed.'
                return JsonResponse({'status':'false','message':str(e), 'code': '500'}, status=200, reason=str(e))
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def saveAndRetrieveBankDetails(request):

    #for searching in db
    if request.method == 'GET':

        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_BankDetails')
        data = ct.findAllData()
        data = dumps(list(data))
        return JsonResponse(data,safe=False)

    # for saving in db
    if request.method == 'POST':

        # set database and collection
        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_BankDetails')

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            finalData['_id'] = finalData['accNo']
            del finalData['permission']
            try:
                ct.saveData(finalData)
            except:
                e = 'Database write Bank details operation failed.'
                return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
            #return HttpResponse("Status code: 200 Bank Details Saved Successfully.")
            return JsonResponse({'status': 'false', 'message': "Bank Details Saved Successfully.", 'code': '200'}, status=200)
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def deleteBankDetails(request):
    #for deleting users
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_BankDetails')
            ct.deleteById(finalData['_id'])
            return HttpResponse("Status code: 200 Bank Details Deleted Successfully.")
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def saveAndRetrieveCompanyDetails(request):

    #for searching in db
    if request.method == 'GET':

        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_CompanyDetails')

        try:
            field = list(request.GET.dict().keys())[0]
            inputData = request.GET[field]
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if field == 'companyCode' or field == 'nameOfCompany':
            query = {field: inputData}
            data = ct.findData(query)
        else:
            # send error message if search parameter extraction failed
            e = 'Invalid data in request, failed to parse.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        data = dumps(list(data))
        return JsonResponse(data,safe=False)

    # for saving in db
    if request.method == 'POST':

        # set database and collection

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            try:
                ct.setDatabase('RG_Database')
                ct.setActiveCollection('Counters')
                counter = ct.getCounter(4)+1

                finalData['companyCode'] = 'RGC'+str(counter)
                finalData['_id'] = finalData['companyCode']
                del finalData['permission']
                ct.setActiveCollection('RG_CompanyDetails')
                ct.saveData(finalData)

                ct.setActiveCollection('Counters')
                ct.updateCounter(counter,4)
            except:
                e = 'Database write Company details operation failed.'
                return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
            return HttpResponse(finalData['_id'])
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def deleteCompany(request):
    #for deleting users
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_CompanyDetails')
            ct.deleteById(finalData['_id'])
            return HttpResponse("Status code: 200 User Deleted Successfully.")
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def changeCompanyDetails(request):

    if request.method == 'POST':

        # set database and collection
        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_CompanyDetails')

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            id = finalData['_id']
            del finalData['_id']
            del finalData['permission']
            ct.replaceUserDetailsById(id, finalData)
            return HttpResponse("Status code: 200 User Details Changed Successfully.")
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))