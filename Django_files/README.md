# RG_ERP

Online ERP project for Ratna Gears.

Main Focus Module :
--------------------

--Quotation
--Purchase Order

Installation and Usage for server:
----------------------------------
1.MAKE SURE INTERNET IS ON.(ONLY FOR DJANGO INSTALLATION).
2.MAKE SURE DJANGO IS INSTALLED ON YOUR SYSTEM (pip install Django).
3.NAVIGATE TO THE FOLDER THAT CONTAINS "Manage.py" FILE.
4.OPEN COMMAND PROMPT (FROM OS OR YOUR IDE) AND TYPE "python manage.py runserver".(MAKE SURE Manage.py IS IN THE FOLDER).
5.THE SERVER SHOULD START AT "127.0.0.1:8000" . JUST VISIT THIS URL IN YOUR BROWSER TO FIND OUR WEBSITE RUNNING.