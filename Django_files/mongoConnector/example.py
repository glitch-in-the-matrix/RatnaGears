'''
This file servers as an example for using Connector.py.
'''

# import connector
import Connector as ct

# For working on mongodb, we need the set active db and collection before
# doing any task.

# set active database
ct.setDatabase('RG_Database')
# set active collection
ct.setActiveCollection('RG_Quotations')

# a trial data for entry in db
trial = {
    "clientDetails": {
        "name": { "firstName": "Santosh", "lastName": "Jha" },
        "mobileNumber": 1234567890,
        "emailId": "sanjha@b.com",
        "address": "Fran Sansisco"
    },

    "productDetails": {
        "nameOfProduct": "Gear",
        "typeOfProduct": "Simple/Assembly",
        "parts": [
            {
                "nameOfPart": "XYZ",
                "processes": [
                    {
                        "nameOfProcess": "Turning",
                        "typeOfProcess": "Inhouse",
                        "M/Chr": 12,
                        "processTime": 7
                    },
                    {
                        "nameOfProcess": "New Process",
                        "typeOfProcess": "Outsourced",
                        "M/Chr": 12,
                        "processTime": 7
                    }
                ],
                "profitOfPart": 50,
                "quantity": 2,
                "HSN": 1234,
                "GST": 18
            },
            {
                "nameOfPart": "LMN",
                "processes": [
                    {
                        "nameOfProcess": "Turning",
                        "typeOfProcess": "Inhouse",
                        "M/Chr": 12,
                        "processTime": 7
                    },
                    {
                        "nameOfProcess": "New Process",
                        "typeOfProcess": "Outsourced",
                        "M/Chr": 12,
                        "processTime": 7
                    }
                ],
                "profitOfPart": 50,
                "quantity": 2,
                "HSN": 1234,
                "GST": 18
            },

            {
                "nameOfPart": "ZXC",
                "processes": [
                    {
                        "nameOfProcess": "Turning",
                        "typeOfProcess": "Inhouse",
                        "M/Chr": 12,
                        "processTime": 7
                    },
                    {
                        "nameOfProcess": "Teeth cutting",
                        "typeOfProcess": "Inhouse",
                        "number of teeth": 12,
                        "Module": 7,
                        "Facewidth": 34,
                        "Factor": 67
                    }
                ],
                "profitOfPart": 50,
                "quantity": 2,
                "HSN": 1234,
                "GST": 18
            }
        ]
    },

    "ourInformation": {
        "date": "1/1/2021",
        "referenceNumber": 1234,
        "termsAndConditions": [
            { "deliveryWithin": "20/1/2021" },
            { "paymentTerms": "value" },
            { "freight": "value" }
        ],
        "attachmentWithConsignment": [
            { "rawMaterialCertificate": True },
            { "ultraSonicTestCertificate": True },
            { "hardnessTestCertificate": False },
            { "internalInspectReport": True }
        ],
        "importantNote": "Very Important",
        "bankAccount": "Yes Bank",
        "contactPersonName": "GHJ"
    }
}

# run the following saveData function to save data to db. Pass in dict as argument.
# ct.saveData(trial)

# nested queries can be done by employing the following syntax
# data = ct.findData({"clientDetails.name.firstName":"Santosh","clientDetails.address":"Fran Sansico"})

# running this line will fetch all the data inside a collection
# data = ct.findAllData()
