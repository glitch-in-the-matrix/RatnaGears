'''
Light Wrapper code for utilizing pymongo to carry out transactions with mongodb
through python.
Contains functions specific to RatnaGears project.
'''
from pymongo import MongoClient
from bson import json_util

# client is the connection object, defines the address for connection
# to mongodb
client = MongoClient('localhost',27017)

# global database object, holds the active db
dataBase = None

# global collection object, holds the active collection
activeCollection = None

# Note: 
# -----
# Since these variables are global, we can directly access them
# to change their values or get their values, but still we declare
# setter and getters below and advice to use them.

# Set client
# ----------

def setClient(host,port):
    global client
    client = MongoClient(host,port)

def setClient(url):
    global client
    client = MongoClient(url)

# Set Database
# -------------
def setDatabase(dbName):
    global dataBase,posts
    dataBase = client[dbName]

# Set active collection
# ----------------------
def setActiveCollection(colName):
    global activeCollection,dataBase
    try:
        activeCollection = dataBase[colName]
    except:
        e = 'Active collection setting failed.'
        return JsonResponse({'status':'false','message':e, 'code': '403'}, status=200, reason=e)

# Save Data
# ---------
def saveData(json):
    global activeCollection
    # data = json_util.loads(json)
    data = json
    if not activeCollection:
        raise Exception('Active collection not set.')
    tempPostID = activeCollection.insert_one(data).inserted_id
    return tempPostID

# Finding elements
# ----------------
def findData(query):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    return activeCollection.find(query)

def findByQuotationNumber(quotationNumber):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"_id":quotationNumber}
    return activeCollection.find(query)

def findByCompanyName(companyName):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"clientDetails.nameOfCompany":companyName}
    return activeCollection.find(query)

def findByDate(date):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"ourInformation.date":date}
    return activeCollection.find(query)

def findAllData():
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    return activeCollection.find()

def findByEmail(email):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"email": email}
    return activeCollection.find(query)
    

def findVpoByPONumber(PO_Number):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"PO_Number":PO_Number}
    return activeCollection.find(query)

def findCpoByOANumber(OA_Number):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"OA_Number":OA_Number}
    return activeCollection.find(query)

def findVpoByCompanyName(vendorCompany):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"vendorCompany":vendorCompany}
    return activeCollection.find(query)

def findVpoByDate(date):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"date":date}
    return activeCollection.find(query)

def getCounter(id):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    if id==1:
        return activeCollection.find({'_id':'counters'})[0]['quotation']
    if id==2:
        return activeCollection.find({'_id':'counters'})[0]['po']
    if id==3:
        return activeCollection.find({'_id':'counters'})[0]['oa']
    if id==4:
        return activeCollection.find({'_id':'counters'})[0]['company']

# Delete elements
# ----------------

def deleteById(_id):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"_id": _id}
    return activeCollection.delete_one(query)

# Update/ reaplace elements
# -------------------------
def replaceUserDetailsById(_id, replacement_data):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"_id": _id}
    return activeCollection.replace_one(query, replacement_data)

def replace(_id, replacement_data):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')

    query = {"_id": _id}
    return activeCollection.replace_one(query, replacement_data)

def updateCounter(value, id):
    global activeCollection
    if not activeCollection:
        raise Exception('Active collection not set.')
    filter = {"_id":'counters'}
    if id==1:
        activeCollection.update_one(filter,{ "$set": { 'quotation': value } })
    if id==2:
        activeCollection.update_one(filter,{ "$set": { 'po': value } })
    if id==3:
        activeCollection.update_one(filter,{ "$set": { 'oa': value } })
    if id==4:
        activeCollection.update_one(filter,{ "$set": { 'company': value } })