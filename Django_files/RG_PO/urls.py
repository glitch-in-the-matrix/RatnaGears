from .views import handleVPO,handleCPO, authorize_PO,deleteVPO
from django.urls import path

urlpatterns = [
    path('handleVPo/', handleVPO,name='handleVPo'),
    path('authorize_PO/', authorize_PO,name='authorize_PO'),
    path('handleCPo/', handleCPO,name='handleCPo'),
    path('deleteVPO/', deleteVPO,name='deleteVPO'),
]