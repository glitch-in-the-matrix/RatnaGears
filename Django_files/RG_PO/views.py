from django.shortcuts import render,HttpResponse
from mongoConnector import Connector as ct
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from bson.json_util import dumps
import json

@csrf_exempt
def handleVPO(request):

	# for searching in db
	if request.method == 'GET':

		try:
			# getting the PO_Number number from request
			field = list(request.GET.dict().keys())[0]
			inputData = request.GET[field]
			print(request.GET.dict())
			isPermited = request.GET.dict()['permission']
		except:
			e = 'Invalid or missing data in request.'
			return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)

		if isPermited=='true' or isPermited==True:

			# querying database (ct refers to db connector instance) for PO_Number
			ct.setDatabase('RG_Database')
			ct.setActiveCollection('RG_VendorPO')
			if field == 'PO_Number':
				data = ct.findVpoByPONumber(inputData)
			elif field == 'vendorCompany':
				data = ct.findVpoByCompanyName(inputData)
			elif field == 'date':
				data = ct.findVpoByDate(inputData)
			else:
				# send error message if search parameter extraction failed
				e = 'Invalid data in request, failed to parse.'
				return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)

			data = dumps(list(data))
			return JsonResponse(data, safe=False)
		else:
		    e = 'Forbidden.'
		    return JsonResponse({'status':'false','message':e, 'code': '403'}, status=200, reason=e)

	if request.method=='POST':

	    try:
		    # get data from QueryDict object
		    dictKeys = list(request.POST.dict().keys())
		    # load it in json format
		    finalData = json.loads(dictKeys[0])
		    isPermited = finalData['permission']
	    except:
		    e = 'Invalid or missing data in request.'
		    return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)
	    
	    if isPermited=='true' or isPermited==True:
	    	#set database and collection
		    ct.setDatabase('RG_Database')
		    ct.setActiveCollection('Counters')
		    counter = ct.getCounter(2)+1
		    finalData['_id'] = 'RGV'+str(counter)
		    finalData['PO_Number'] = finalData['_id'] 
		    del finalData['permission']
		    ct.setActiveCollection('RG_VendorPO')
		    try:
		    	ct.saveData(finalData)
		    except:
			    e = 'Database write Vendor PO operation failed.'
			    return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
		    ct.setActiveCollection('Counters')
		    ct.updateCounter(counter,2)

		    return HttpResponse(finalData['_id'])
	    else:
		    e = 'Forbidden.'
		    return JsonResponse({'status':'false','message':e, 'code': '403'}, status=200, reason=e)
	else:
		e = 'Invalid request method.'
		return JsonResponse({'status':'false','message':e, 'code': '405'}, status=200, reason=e)

@csrf_exempt
def deleteVPO(request):

	if request.method=='POST':

	    try:
		    # get data from QueryDict object
		    dictKeys = list(request.POST.dict().keys())
		    # load it in json format
		    finalData = json.loads(dictKeys[0])
		    isPermited = finalData['permission']
	    except:
		    e = 'Invalid or missing data in request.'
		    return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)
	    
	    if isPermited=='true' or isPermited==True:
	    	po_number = finalData['po']
	    	try:
		    	ct.setDatabase('RG_Database')
		    	ct.setActiveCollection('RG_VendorPO')
		    	data = ct.findVpoByPONumber(po_number)
		    	data = list(data)[0]
	    	except:
		    	e = 'VPO does not exist, cannot delete.'
		    	return JsonResponse({'status':'false','message':e, 'code': '410'}, status=200, reason=e)
	    	if data['authorized']==False or data['authorized']=='false':
		    	try:
		    		ct.setActiveCollection('RG_VendorPO')
		    		ct.deleteById(po_number)
		    		return HttpResponse(po_number)
		    	except:
		    		e = 'Delete VPO operation failed.'
			    	return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
	    	else:
		    	e = 'Cannot delete approved Vendor PO.'
		    	return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)

	    else:
		    e = 'Forbidden.'
		    return JsonResponse({'status':'false','message':e, 'code': '403'}, status=200, reason=e)
	else:
		e = 'Invalid request method.'
		return JsonResponse({'status':'false','message':e, 'code': '405'}, status=200, reason=e)


@csrf_exempt
def authorize_PO(request):

	if request.method == 'POST':
		try:
			# get data from QueryDict object
			dictKeys = list(request.POST.dict().keys())
			# load it in json format
			finalData = json.loads(dictKeys[0])
			isPermited = finalData['permission']
		except:
			e = 'Invalid or missing data in request.'
			return JsonResponse({'status': 'false', 'message': e, 'code': '400'}, status=200, reason=e)

		if isPermited == 'true' or isPermited == True:
			ct.setDatabase('RG_Database')
			ct.setActiveCollection('RG_VendorPO')

			del finalData['permission']
			try:
				ct.replace(finalData['PO_Number'], finalData)
				return HttpResponse(finalData['_id'])
			except:
				e = 'Database write Vendor PO operation failed.'
				return JsonResponse({'status': 'false', 'message': e, 'code': '500'}, status=200, reason=e)

		else:
			e = 'Forbidden.'
			return JsonResponse({'status': 'false', 'message': e, 'code': '403'}, status=200, reason=e)

	else:
		e = 'Invalid request method.'
		return JsonResponse({'status': 'false', 'message': str(e), 'code': '405'}, status=200, reason=str(e))





@csrf_exempt
def handleCPO(request):

	if request.method == 'GET':

		try:
			# getting the OA_Number number from request
			field = list(request.GET.dict().keys())[0]
			inputData = request.GET[field]
		except:
		    e = 'Invalid or missing data in request.'
		    return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)

		# querying database (ct refers to db connector instance) for OA_Number
		ct.setDatabase('RG_Database')
		ct.setActiveCollection('RG_CustomerPO')

		if field == 'OA_Number':
			data = ct.findCpoByOANumber(inputData)
		else:
			# send error message if search parameter extraction failed
		    e = 'Invalid data in request, failed to parse.'
		    return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)

		data = dumps(list(data))
		return JsonResponse(data, safe=False)

	if request.method=='POST':
		
	    try:
		    # get data from QueryDict object
		    dictKeys = list(request.POST.dict().keys())
		    # load it in json format
		    finalData = json.loads(dictKeys[0])
		    isPermited = finalData['permission']
	    except:
		    e = 'Invalid or missing data in request.'
		    return JsonResponse({'status':'false','message':e, 'code': '400'}, status=200, reason=e)
	    
	    if isPermited=='true' or isPermited==True:
	    	#set database and collection
		    ct.setDatabase('RG_Database')
		    ct.setActiveCollection('Counters')
		    counter = ct.getCounter(3)+1
		    finalData['_id'] = 'RGO'+str(counter)
		    finalData['OA_Number'] = finalData['_id'] 
		    del finalData['permission']
		    ct.setActiveCollection('RG_CustomerPO')
		    try:
		    	ct.saveData(finalData)
		    except:
			    e = 'Database write Customer PO operation failed.'
			    return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
		    ct.setActiveCollection('Counters')
		    ct.updateCounter(counter,3)

		    return HttpResponse(finalData['_id'])
	    else:
		    e = 'Forbidden.'
		    return JsonResponse({'status':'false','message':e, 'code': '403'}, status=200, reason=e)
	else:
		e = 'Invalid request method.'
		return JsonResponse({'status':'false','message':e, 'code': '405'}, status=200, reason=e)

