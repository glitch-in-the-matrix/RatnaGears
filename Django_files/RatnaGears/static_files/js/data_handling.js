// function to operate Product Type radio buttons
function chooseProductType(radioButton){
	if (radioButton.checked && radioButton.id == "customRadioInline2") {
		document.getElementById('AddPartButton').style.display = 'block';
	}
	else{
		document.getElementById('AddPartButton').style.display = 'none';	
	}
}

// function to operate Process Type radio buttons
function chooseProcessType(radioButton){
	var mainString = radioButton.value.split(' ');
	var token = parseInt(mainString[0]);
	var partNumber = parseInt(mainString[1]);
	var processNumber = parseInt(mainString[2]);
	console.log(radioButton.vaue);
	console.log('part number:'+partNumber);
	console.log('process number:'+processNumber);
	console.log('token:'+token);
	var prefix = 'part'+partNumber;
	var parentContainer = document.getElementById(prefix+"processInputContainer"+processNumber);
	while(parentContainer.firstChild){
		parentContainer.firstChild.remove();
	}
	// check if its inhouse
	if (radioButton.checked && token===1) {
		var selector = document.getElementById(prefix+'processSelector'+processNumber);
		var optionSelected = parseInt(selector.options[selector.selectedIndex].value);
		console.log(optionSelected);
		if (optionSelected>0 && optionSelected<3) {
			var noOfTeeth = document.createElement("input");
			var module_ = document.createElement("input");
			var faceWidth = document.createElement("input");
			var factor = document.createElement("input");

			noOfTeeth.type 	='text';
			module_.type 	='text';
			faceWidth.type 	='text';
			factor.type 	='text';

			noOfTeeth.placeholder = 'Number of teeth';
			module_.placeholder = 'Module';
			faceWidth.placeholder = 'Facewidth';
			factor.placeholder = 'Factor';

			parentContainer.appendChild(noOfTeeth);
			parentContainer.appendChild(module_);
			parentContainer.appendChild(faceWidth);
			parentContainer.appendChild(factor);

		}
		else if (optionSelected>2 && optionSelected<11) {
			var rate = document.createElement("input");
			var time = document.createElement("input");

			rate.type 	='text';
			time.type 	='text';

			rate.placeholder = 'M/C hour rate';
			time.placeholder = 'Process time';

			parentContainer.appendChild(rate);
			parentContainer.appendChild(time);
		}
		else if (optionSelected>10 && optionSelected<15) {
			var rate = document.createElement("input");
			var weight = document.createElement("input");

			rate.type 	='text';
			weight.type 	='text';

			rate.placeholder = 'Process rate';
			weight.placeholder = 'Weight';

			parentContainer.appendChild(rate);
			parentContainer.appendChild(weight);
		}
		else if (optionSelected>14 && optionSelected<18) {
			var rate = document.createElement("input");
			var number = document.createElement("input");

			rate.type 	='text';
			number.type 	='text';

			rate.placeholder = 'Rate per operation';
			number.placeholder = 'Number of operation';

			parentContainer.appendChild(rate);
			parentContainer.appendChild(number);
		}
	}
	else{

		var companyName = document.createElement("input");
		var extraCharges = document.createElement("input");

		companyName.type 	='text';
		extraCharges.type 	='text';

		companyName.placeholder = 'Enter Name of Company';
		extraCharges.placeholder = 'Enter Extra Charges Incurred';

		parentContainer.appendChild(companyName);
		parentContainer.appendChild(extraCharges); 
	}

	// if (radioButton.checked && radioButton.id == "customRadioInline3") {
	// 	document.getElementById('Inhouse').style.display = 'block';
	// 	document.getElementById('Outhouse').style.display = 'none';
	// }
	// else{
	// 	document.getElementById('Inhouse').style.display = 'none';
	// 	document.getElementById('Outhouse').style.display = 'block';
	// }
}

//function to check if element is valid
	const isValidElement = element=>{
		return element.value && element.name;
	};

// convert form data to raw json
	const formToJSON = elements=> [].reduce.call(elements,(data,element)=>{

		if (isValidElement(element)) {
			if (element.getAttribute('type')==='text' || 
				element.getAttribute('type')==='number'||
				element.getAttribute('type')==='email') {
				data[element.name] = element.value;
			}
		}
		return data;

	},{});
//  function to send json object to server
	$(document).on('submit','#msform',function(e){
		e.preventDefault();
		const form = document.getElementById('msform');
		let FinalData = formToJSON(form.elements);
		FinalData = JSON.stringify(FinalData);
		$.ajax({
			type:'POST',
			url: "http://127.0.0.1:8000/quotations/recieveJSON/",
			data:{
				json:FinalData,
				csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken').val()
			},
			success:function(){}
		});
	});

	// add new part function
	function addPart(){
		var partsContainer = document.getElementById('partsContainer');
		var partsCounter = parseInt(document.getElementById('partsCounter').className)+1;
		var prefix = 'part'+partsCounter;
		var part = document.createElement('div');
		part.id = prefix;

		// adding process counter
		var processCounter = document.createElement('label');
		processCounter.style = 'display: none;';
		processCounter.className = '0';
		processCounter.id = prefix+'processCounter';
		part.appendChild(processCounter);

		// adding part banner
		var heading = document.createElement('h4');
		heading.innerHTML = 'Part'+partsCounter;
		heading.style = 'text-align: center;';
		part.appendChild(heading);

		// adding name of part field
		var nameOfPart = document.createElement('input');
		nameOfPart.type = 'text';
		nameOfPart.placeholder = 'Name of part';
		nameOfPart.id = prefix+'nameOfPart';
		part.appendChild(nameOfPart);

		// adding process container
		var partProcessContainer = document.createElement('div');
		partProcessContainer.id = prefix+'processContainer';
		partsContainer.appendChild(part);
		part.appendChild(partProcessContainer);
		document.getElementById('partsCounter').className = partsCounter;

		//adding add process button 
		var processButton = document.createElement('button');
		processButton.type = 'button';
		processButton.className = "btn btn-info";
		processButton.id = prefix+'processButton';
		processButton.innerHTML = 'Add Process';
		processButton.value = partsCounter;
		processButton.onclick = function(){addProcess(processButton);};
		part.appendChild(processButton);

		// adding extra input fields
		var placeholderText=['Enter Profit Percentage for this part',
							 'Enter quantity of this part',
							 'HSN/SAC',
							 'GST (%)'] ;
		var ids = [prefix+'profitPercent',
				   prefix+'quantity',
				   prefix+'hsn',
				   prefix+'gst'];
		for (var i = 0; i < placeholderText.length; i++) {
			var ip = document.createElement('input');
			ip.placeholder = placeholderText[i];
			ip.id = ids[i];
			part.appendChild(ip);
		}
		part.appendChild(document.createElement('br'));
		part.appendChild(document.createElement('br'));
		part.appendChild(document.createElement('br'));
	}

	// add process function
	function addProcess(button){

		var partsCounter = parseInt(button.value);
		var prefix = 'part'+partsCounter; 
		var processCounter = parseInt(document.getElementById(prefix+'processCounter').className)+1;
		// var partsCounter = parseInt(document.getElementById('partsCounter').className);
		var parentContainer = document.getElementById('part'+partsCounter+'processContainer');
		console.log('partsCounter:'+partsCounter);

		// addin heading for process
		var processHeading = document.createElement('h5');
		processHeading.style = 'color:#2C3E50';
		processHeading.innerHTML = 'Part '+partsCounter+' : '+'Process '+processCounter;
		parentContainer.appendChild(processHeading);
		parentContainer.appendChild(document.createElement('br'));

		// adding the process dropdown menu
		var select = document.createElement('select');
		select.className = 'custom-select';
		var defaultOption = document.createElement('option');
		defaultOption.innerHTML = 'Choose Process Type';
		defaultOption.value = '0';
		select.appendChild(defaultOption);

		var processNames = ['Teeth cutting','Teeth grinding','Turning','Milling','Grinding','Wire cutting',
		'Chrome plating','CMM','CNC turning','VMC machining','Raw material','Phosphating','Blackodising',
		'Hardening','Drilling','Tapping','Keyway','Not in the list...'];
		for (var i = 0; i < processNames.length; i++) {
			var option = document.createElement('option');
			option.value = i+1;
			option.innerHTML = processNames[i];
			select.appendChild(option);
		}

		select.id = prefix+'processSelector'+processCounter;
		parentContainer.appendChild(select);
		parentContainer.appendChild(document.createElement('br'));

		// adding process type radio boxes
		var ptDiv = document.createElement('div');
		ptDiv.innerHTML = 'Type Of Process';
		ptDiv.style = 'color: #2C3E50; font-size: 16px;';
		parentContainer.appendChild(ptDiv);
		parentContainer.appendChild(document.createElement('br'));
		// radio button 1
		var rbDiv1 = document.createElement('div');
		rbDiv1.className = 'custom-control custom-radio custom-control-inline';
		parentContainer.appendChild(rbDiv1);
		var rbIP1 = document.createElement('input');
		rbIP1.type = 'radio';
		// rbIP1.id = 'customRadioInline'+((processCounter*2)+1);
		rbIP1.id = prefix+'Inhouse'+((processCounter));
		rbIP1.name = prefix+'customRadioInline'+((processCounter));
		rbIP1.className = 'custom-control-input';
		rbIP1.value = '1 '+partsCounter+' '+processCounter;
		rbIP1.onclick=function(){chooseProcessType(rbIP1)}; 
		rbDiv1.appendChild(rbIP1);
		var rbLbl1 = document.createElement('label');
		rbLbl1.className = "custom-control-label";
		rbLbl1.htmlFor = rbIP1.id;
		rbLbl1.innerHTML = "Inhouse";
		rbDiv1.appendChild(rbLbl1);
		// radio button 2
		var rbDiv2 = document.createElement('div');
		rbDiv2.className = 'custom-control custom-radio custom-control-inline';
		parentContainer.appendChild(rbDiv2);
		var rbIP2 = document.createElement('input');
		rbIP2.type = 'radio';
		// rbIP2.id = 'customRadioInline'+((processCounter*2)+2);
		rbIP2.id = prefix+'Outhouse'+((processCounter));
		rbIP2.name = rbIP1.name;
		rbIP2.className = 'custom-control-input';
		rbIP2.value = '2 '+partsCounter+' '+processCounter;
		rbIP2.onclick=function(){chooseProcessType(rbIP2)}; 
		rbDiv2.appendChild(rbIP2);
		var rbLbl2 = document.createElement('label');
		rbLbl2.className = "custom-control-label";
		rbLbl2.htmlFor = rbIP2.id;
		rbLbl2.innerHTML = "Outhouse";
		rbDiv2.appendChild(rbLbl2);
		parentContainer.appendChild(document.createElement('br'));
		parentContainer.appendChild(document.createElement('br'));

		// adding the process inputs container
		var processInputContainer = document.createElement('div');
		processInputContainer.id = prefix+"processInputContainer"+processCounter;
		parentContainer.appendChild(processInputContainer);

		parentContainer.appendChild(document.createElement('br'));
		parentContainer.appendChild(document.createElement('br'));

		document.getElementById(prefix+'processCounter').className = processCounter;
	}