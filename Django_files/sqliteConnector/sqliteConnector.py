import sqlite3

#create connection to db and a cursor into the db
def startConnection():
	connection = sqlite3.connect('counters.db')
	cursor = connection.cursor()
	return connection,cursor

def endConnection(connection):
	connection.close()


def getQuotationCounter(cursor):

	return cursor.execute('SELECT count FROM counters WHERE name = "quotation"').fetchall()[0][0]
	 
def getPOCounter(cursor):

	return cursor.execute('SELECT count FROM counters WHERE name = "po"').fetchall()[0][0]

def getOACounter(cursor):

	return cursor.execute('SELECT count FROM counters WHERE name = "oa"').fetchall()[0][0]


def updateQuotationCounter(connection,cursor,counter):
	cursor.execute('UPDATE counters SET count = ? WHERE name = "quotation"',(counter,))
	connection.commit()

def updatePOCounter(connection,cursor,counter):
	cursor.execute('UPDATE counters SET count = ? WHERE name = "po"',(counter,))
	connection.commit()

def updateOACounter(connection,cursor,counter):
	cursor.execute('UPDATE counters SET count = ? WHERE name = "oa"',(counter,))
	connection.commit()