from django.apps import AppConfig


class RgQuotationsConfig(AppConfig):
    name = 'RG_Quotations'
