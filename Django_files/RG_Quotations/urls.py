from django.urls import path
from .views import handleQuotation,editQuotation,fetchRevisions,finalizeQuotation,deleteQuotation
urlpatterns = [
    path('handleQuotation/', handleQuotation,name='handleQuotation'),
    path('editQuotation/', editQuotation,name='editQuotation'),
    path('fetchRevisions/', fetchRevisions,name='fetchRevisions'),
    path('finalizeQuotation/', finalizeQuotation,name='finalizeQuotation'),
    path('deleteQuotation/', deleteQuotation,name='deleteQuotation'),
]