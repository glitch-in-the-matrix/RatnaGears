from mongoConnector import Connector as ct
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from bson.json_util import dumps
import json


@csrf_exempt
def handleQuotation(request):

    # for searching in db
    if request.method == 'GET':

        # retrieving the data from request
        try:
            field = list(request.GET.dict().keys())[0]
            inputData = request.GET[field]
            isPermited = request.GET.dict()['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:

            # querying database (ct refers to db connector instance) for given search parameter
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_Quotations')
            if field == 'quotationNumber':
                data = ct.findByQuotationNumber(inputData)
            elif field == 'nameOfCompany':
                data = ct.findByCompanyName(inputData)
            elif field == 'date':
                data = ct.findByDate(inputData)
            else:
                # send error message if search parameter extraction failed
                e = 'Invalid data in request, failed to parse.'
                return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

            # return data from db to frontend
            data = dumps(list(data))
            return JsonResponse(data,safe=False)
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
        


    # for saving in db
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
            quotationCount = finalData['count']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
        
            try:
                #set database and collection
                ct.setDatabase('RG_Database')
                ct.setActiveCollection('Counters')
                counter = ct.getCounter(1)+1
                # print(counter)
                finalData['_id'] = 'RGQ'+str(counter)
                finalData['quotationNumber'] = finalData['_id']+'R'+str(quotationCount) 
                del finalData['permission']
                try:
                    ct.setActiveCollection('RG_Quotations')
                    # print(finalData)
                    ct.saveData(finalData)
                except:
                    e = 'Database write Quotation operation failed.'
                    return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
                ct.setActiveCollection('Counters')
                ct.updateCounter(counter,1)
            except:
                e = 'Quotation saving failed.'
                return JsonResponse({'status':'false','message':str(e), 'code': '500'}, status=200, reason=str(e))
            
            return HttpResponse(finalData['_id'])
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def deleteQuotation(request):
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
        
            try:
                quotationNumber = finalData['quotationNumber']
                ct.setDatabase('RG_Database')
                ct.setActiveCollection('RG_Quotations')
                data = ct.findByQuotationNumber(quotationNumber)
                data = list(data)[0]
            except:
                e = 'Quotation does not exist, cannot delete.'
                return JsonResponse({'status':'false','message':str(e), 'code': '410'}, status=200, reason=str(e))

            if data['finalized'] == 0:
                try:
                    ct.setActiveCollection('RG_Quotations')
                    ct.deleteById(quotationNumber)
                    ct.setActiveCollection('RG_Revisions')
                    ct.deleteById(quotationNumber)
                    return HttpResponse(quotationNumber)
                except:
                    e = 'Delete Quotation operation failed.'
                    return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
            
            else:
                e = 'Cannot delete finalized Quotation.'
                return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)
            
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))
    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))


@csrf_exempt
def editQuotation(request):

    # for saving in db
    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            quotationNumber = finalData['_id']
            #countFinal = finalData['count']
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e)) 

        if isPermited=='true' or isPermited==True:

            #retrieve latest saved quotation(saved in RG_Quotation)
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_Quotations')
            latestSaved = ct.findByQuotationNumber(quotationNumber)
            latestSaved = list(latestSaved)[0]
            #countLatestSaved = latestSaved['count']
            # try to get revision for given quotation number
            ct.setActiveCollection('RG_Revisions')
            revision = ct.findByQuotationNumber(quotationNumber)
            revision = list(revision)

            del finalData['permission']
            count = 0
            # if revision exists then add the new revision, replace it and replace 
            # latest revision
            if len(revision)>0:
                try:
                    revision = revision[0]
                    count = len(revision)-2
                except:
                    e = 'Revision count generation failed.'
                    return JsonResponse({'status':'false','message':str(e), 'code': '500'}, status=200, reason=str(e))
                index = 'R'+str(count)
                revision[index] = latestSaved
                ct.setActiveCollection('RG_Revisions')
                ct.replace(quotationNumber,revision)    

            # else create a revision
            else:
                ct.setActiveCollection('RG_Revisions')
                data = {'_id':quotationNumber,'quotationNumber':quotationNumber,'R0':latestSaved}
                try:
                    ct.saveData(data)
                except:
                    e = 'Database create Revision operation failed.'
                    return JsonResponse({'status':'false','message':e, 'code': '500'}, status=200, reason=e)

            ct.setActiveCollection('RG_Quotations')
            finalData['count'] = count + 1
            finalData['quotationNumber'] = finalData['_id'] + 'R' + str(finalData['count'])
            ct.replace(quotationNumber,finalData)
            return JsonResponse({ 'quotationNumber': finalData['quotationNumber'], 'count': finalData['count']})
        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))

@csrf_exempt
def fetchRevisions(request):
    # for saving in db
    if request.method == 'GET':

        try:
            # get data from QueryDict object
            dictKeys = list(request.GET.dict().values())
            # load it in json format
            quotationNumber = dictKeys[0]
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        # querying database (ct refers to db connector instance) for quotation number
        ct.setDatabase('RG_Database')
        ct.setActiveCollection('RG_Revisions')

        data = ct.findByQuotationNumber(quotationNumber)
        data = dumps(list(data))
        return JsonResponse(data,safe=False)

    else:
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))


@csrf_exempt
def finalizeQuotation(request):

    if request.method == 'POST':

        try:
            # get data from QueryDict object
            dictKeys = list(request.POST.dict().keys())
            # load it in json format
            finalData = json.loads(dictKeys[0])
            quotationNumber = finalData['_id']
            countFinal = finalData['count']
            isPermited = finalData['permission']
        except:
            e = 'Invalid or missing data in request.'
            return JsonResponse({'status':'false','message':str(e), 'code': '400'}, status=200, reason=str(e))

        if isPermited=='true' or isPermited==True:
            #retrieve latest saved quotation
            ct.setDatabase('RG_Database')
            ct.setActiveCollection('RG_Quotations')
            print(quotationNumber)
            latestSaved = ct.findByQuotationNumber(quotationNumber)
            latestSaved = list(latestSaved)[0]
            countLatestSaved = latestSaved['count']
            del finalData['permission']


            if countLatestSaved != countFinal:

                ct.setActiveCollection('RG_Revisions')
                revision = ct.findByQuotationNumber(quotationNumber)
                revision = list(revision)

                try:
                    revision = revision[0]
                    count = len(revision)-2
                except:
                    e = 'Revision count generation failed.'
                    return JsonResponse({'status':'false','message':str(e), 'code': '500'}, status=200, reason=str(e))
                index = 'R'+str(count)
                revision[index] = latestSaved
                ct.setActiveCollection('RG_Revisions')
                ct.replace(quotationNumber,revision)
                
            finalData['count'] = countLatestSaved
            ct.setActiveCollection('RG_Quotations')
            ct.replace(quotationNumber,finalData)

            return HttpResponse(finalData['quotationNumber'])

        else:
            e = 'Forbidden.'
            return JsonResponse({'status':'false','message':str(e), 'code': '403'}, status=200, reason=str(e))

    else:
        
        e = 'Invalid request method.'
        return JsonResponse({'status':'false','message':str(e), 'code': '405'}, status=200, reason=str(e))


