

'''Run this file only once, when setting up database for first time'''

import sqlite3

#create connection to db and a cursor into the db
connection = sqlite3.connect('counters.db')
cursor = connection.cursor()

#create table
# cursor.execute('CREATE TABLE counters (name TEXT, count INTEGER)')

# #insert the required data

# cursor.execute('INSERT INTO counters VALUES ("quotation",0)')
# cursor.execute('INSERT INTO counters VALUES ("po",0)')
# cursor.execute('INSERT INTO counters VALUES ("oa",0)')
# connection.commit()

# # update required data
# #####Use these lines of code to to reset the counters to 0,but be cautious######
# cursor.execute('UPDATE counters SET count = ? WHERE name = "quotation"',(0,))
# cursor.execute('UPDATE counters SET count = ? WHERE name = "po"',(0,))
# cursor.execute('UPDATE counters SET count = ? WHERE name = "oa"',(0,))
# connection.commit()

rows = cursor.execute('SELECT name, count FROM counters').fetchall()
print(rows)
